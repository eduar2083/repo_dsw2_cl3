<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CL 3 - DSW II</title>
    <link rel="stylesheet" href="./resources/libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="./resources/libs/font-awesome-5-css/css/all.min.css">
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="card">
            <div class="card-header bg-primary text-white text-center">
                <h4>Clientes</h4>
            </div>
            <div class="card-body">
                <nav class="navbar navbar-light bg-light justify-content-between">
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalCliente"
                        data-whatever="@mdo">
                        <i class="fas fa-plus mr-1"></i> Nuevo
                    </button>
                    <form class="form-inline" id="frmBusqueda" autocomplete="off">
                        <input class="form-control mr-sm-2" type="search" placeholder="edad m�nima"
                            aria-label="edad m�nima" id="edadMinima">
                        <input class="form-control mr-sm-2" type="search" placeholder="edad m�xima"
                            aria-label="edad m�xima" id="edadMaxima">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                        <button class="btn btn-outline-secondary my-2 my-sm-0" type="button" id="btnLimpiar">Limpiar</button>
                    </form>
                </nav>
                <table class="table table-stripped table-hover table-info mt-2" id="tbListado">
					<thead>
						<tr>
							<td>Id</td><td>Nombre</td><td>Apellidos</td><td>Dni</td><td>Edad</td>
						</tr>
					</thead>
					<tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCliente" tabindex="-1" role="dialog" aria-labelledby="modalClienteLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="frmRegistro" autocomplete="off">
                    <div class="modal-header bg-primary text-white">
                        <h5 class="modal-title" id="exampleModalLabel">Nuevo cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="nombre" class="col-form-label">Nombre:</label>
                                <input type="text" class="form-control" id="nombre" name="nombre">
                                <span class="error" for="nombre"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidos" class="col-form-label">Apellidos:</label>
                                <input type="text" class="form-control" id="apellidos" name="apellidos">
                                <span class="error" for="apellidos"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="dni" class="col-form-label">Dni:</label>
                                <input type="text" class="form-control" id="dni" name="dni">
                                <span class="error" for="dni"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="edad" class="col-form-label">Edad:</label>
                                <input type="number" class="form-control" id="edad" name="edad">
                                <span class="error" for="edad"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="./resources/libs/jquery/dist/jquery.min.js"></script>
    <script src="./resources/libs/popperjs/umd/popper.min.js"></script>
    <script src="./resources/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="./resources/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="./resources/libs/axios/axios.min.js"></script>
    <script src="./resources/js/main.js"></script>
</body>

</html>