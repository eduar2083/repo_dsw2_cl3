$(function() {
    // Variables globales
    window.EndPoint = "http://localhost:8080/ProyComercial/api";
    /*
    window.Auth = {
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
    };*/

    fInicializar();
});

function fInicializar() {
    fConfigurarFormulario();
    fConfiguraEventos();
    
    fLimpiarFormularioBusqueda();
    fListarDatos();
}

function fConfigurarFormulario() {
    $("#frmRegistro").validate({
        rules: {
          nombre: {
              required: true,
              maxlength: 50
          },
          apellidos: {
              required: true,
              maxlength: 50
          },
          dni: {
              required: true,
              minlength: 8,
              maxlength: 8,
              digits: true
          },
          edad: {
              required: true,
              maxlength: 3,
              digits: true
          }
        },
        messages: {
            nombre: {
                required: "Debe ingresar nombre",
                maxlength: "Maximo {0} caracteres"
            },
            apellidos: {
                required: "Debe ingresar apellidos",
                maxlength: "Maximo {0} caracteres"
            },
            dni: {
                required: "Debe ingresar dni",
                minlength: "Debe tener {0} dígitos",
                maxlength: "Debe tener {0} dígitos",
                number: "Debe ingresar un dni válido"
            },
            edad: {
                required: "Debe ingresar edad",
                digits: "Debe ingresar un entero válido"
            }
        }
  });
}

function fConfiguraEventos() {
    $('#frmRegistro').bind('submit', frmRegistro_submit);
    $('#frmBusqueda').bind('submit', function(e) {
    	e.preventDefault();
    	fListarDatos();
    });
    
    $('#btnLimpiar').bind('click', function(){
    	fLimpiarFormularioBusqueda();
    	fListarDatos();
    });
    
    $('#modalCliente').on('shown.bs.modal', function (e) {
    	fLimpiarFormularioRegistro();
    });
}

function frmRegistro_submit(e) {
    var isValid = $('#frmRegistro').valid();

        if (isValid) {
            e.preventDefault();
            
            $('#modalCliente').modal('hide');
            
            let obj = {
                "nombre": $('#nombre').val(),
                "apellidos": $('#apellidos').val(),
                "dni": $('#dni').val(),
                "edad": $('#edad').val()
            };

            var json = JSON.stringify(obj);

            axios.post(window.EndPoint + "/Clientes",
                obj//,
                //window.Auth
            )
            .then(function(response) {
                swal("Correcto!", "Se ha registrado!", "success");
                fLimpiarFormularioRegistro();
                fListarDatos();
            })
            .catch(function(response) {
                swal("Ha ocurrido un error!", "Consulte con el departamente de sistemas", "error");
            });
        }
}

function fLimpiarFormularioRegistro() {
	$('#frmRegistro input').val('');
}

function fLimpiarFormularioBusqueda() {
	$('#edadMinima,#edadMaxima').val('');
}

function fListarDatos() {
	let edadMinima = $('#edadMinima').val();
	let edadMaxima = $('#edadMaxima').val();
	
	let params = {
		min: edadMinima == '' ? -1 : parseInt(edadMinima),
		max: edadMaxima == '' ? -1 : parseInt(edadMaxima)
	};
	
	axios.get(window.EndPoint + '/Clientes/rangoedad/' + params.min + '/' + params.max)
	.then(function(response) {
        let lista = response.data;
        
        $('#tbListado tbody').html('');
        lista.map(function(e, i) {
        	$('#tbListado tbody').append(`<tr><td>${e.id}</td><td>${e.nombre}</td><td>${e.apellidos}</td><td>${e.dni}</td><td>${e.edad}</td></tr>`);
        });
    })
    .catch(function(response) {
        swal("Ha ocurrido un error!", "Consulte con el departamente de sistemas", "error");
    });
}