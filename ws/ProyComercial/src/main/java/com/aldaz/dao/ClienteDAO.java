package com.aldaz.dao;

import java.util.List;

import com.aldaz.entity.ClienteBean;

public interface ClienteDAO {
	
	public ClienteBean insertar(ClienteBean bean);
	
	public List<ClienteBean> filtrarXRangoEdad(int minimo, int maximo);
}
