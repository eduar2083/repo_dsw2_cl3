package com.aldaz.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.aldaz.entity.ClienteBean;

@Repository
public class ClienteDAOImpl implements ClienteDAO {

	public ClienteBean insertar(ClienteBean bean) {
		Session session = null;
		Transaction transaction = null;
		
        try {
        	session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(bean);
            transaction.commit();
            
            return bean;
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
            throw new RuntimeException(e);
        }
	}

	@SuppressWarnings("unchecked")
	public List<ClienteBean> filtrarXRangoEdad(int minimo, int maximo) {
		Session session = null;
		Query query = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			
			String hql = "from ClienteBean Where edad between ?1 and ?2";
			query = session.createQuery(hql);
			query.setParameter(1, minimo == -1 ? 0 : minimo);
			query.setParameter(2, maximo == -1 ? 9999 : maximo);
			
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
