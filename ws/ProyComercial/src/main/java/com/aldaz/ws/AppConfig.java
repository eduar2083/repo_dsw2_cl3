package com.aldaz.ws;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.aldaz.service")
@ComponentScan("com.aldaz.dao")
public class AppConfig {

}
