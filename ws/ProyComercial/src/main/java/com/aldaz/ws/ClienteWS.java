package com.aldaz.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.aldaz.entity.ClienteBean;
import com.aldaz.service.ClienteService;

@Path("/Clientes")
public class ClienteWS {
	
	private AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
	private ClienteService clienteService = ctx.getBean("clienteServiceImpl", ClienteService.class);

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertar(ClienteBean bean) {
		try {
			return Response.ok(clienteService.insertar(bean)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
	
	@Path("/rangoedad/{min}/{max}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response filtrarXPrecio(@PathParam("min") int minimo, @PathParam("max") int maximo) {
		try {
			return Response.ok(clienteService.filtrarXRangoEdad(minimo, maximo)).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}
