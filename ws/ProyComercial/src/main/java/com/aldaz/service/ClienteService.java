package com.aldaz.service;

import java.util.List;

import com.aldaz.entity.ClienteBean;

public interface ClienteService {
	
	public ClienteBean insertar(ClienteBean bean);
	
	public List<ClienteBean> filtrarXRangoEdad(int minimo, int maximo);
}
