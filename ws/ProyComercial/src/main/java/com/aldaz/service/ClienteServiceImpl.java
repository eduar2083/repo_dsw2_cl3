package com.aldaz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aldaz.dao.ClienteDAO;
import com.aldaz.entity.ClienteBean;

@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteDAO dao;

	public ClienteBean insertar(ClienteBean bean) {
		return dao.insertar(bean);
	}

	public List<ClienteBean> filtrarXRangoEdad(int minimo, int maximo) {
		return dao.filtrarXRangoEdad(minimo, maximo);
	}
}
